import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Image,
  Button,
  NavigatorIOS
} from 'react-native';

import PinLeft from './10._Zugriffsberechtigung_images/PinLeft.png'
import Group2 from './10._Zugriffsberechtigung_images/Group2.png'
import Page1 from './10._Zugriffsberechtigung_images/Page1.png'



export default class NavigatorIOSApp extends Component {
  render() {
    return (
      <NavigatorIOS
        initialRoute={{
          component: Main,
          title: 'Deine Fotos',
        }}
        style={{flex: 1}}
      />
    );
  }
}





class Main extends Component {

  render() {
    return (
      <ScrollView style={{
        flex: 1, alignSelf: 'stretch', 
        backgroundColor: '#FFFFFF'}}>
        <View style={styles._10Zugriffsberechtigung}>
          <View style={styles.Group3}>
            <Image source={Group2} style={styles.Group2} />
          </View>
          <Text style={styles.Leiderhabewirkein}>
            <Text>Leider habe wir keinen Zugriff </Text>{'\n'}
            <Text>auf deine Bilder.</Text>{'\n'}
          </Text>
          <Text style={styles.DieZugriffserlaubni}>
            <Text>Die Zugriffserlaubnis ist nötig, um </Text>{'\n'}
            <Text>Bilder aus deiner Bibliothek für die </Text>{'\n'}
            <Text>Fotosuche zu nutzen. Aktiviere </Text>{'\n'}
            <Text>den Zugriff in den Einstellungen, </Text>{'\n'}
            <Text>um fortzufahren.</Text>{'\n'}
          </Text>
          <View style={styles.Group4Copy}>
            <View style={styles.Group}>
              <View style={styles.Rectangle6}>
                <Button style={styles.ZudenEinstellungen}
                title="Zu den Einstellungen"
                color="#C5175F"
                ></Button>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  Group3: {
    alignSelf: 'center',
    marginTop: 129,
    width: 116,
    height: 116,
    alignItems: 'center',
    justifyContent: 'center'
  },
  Group2: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  Leiderhabewirkein: {
    backgroundColor: 'transparent',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#C6C6C6',
    alignSelf: 'center',
    marginTop: 32,
    textAlign: 'center'
  },
  DieZugriffserlaubni: {
    backgroundColor: 'transparent',
    fontSize: 17,
    fontWeight: 'normal',
    color: '#BFBFBF',
    alignSelf: 'center',
    textAlign: 'center'
  },
  Group4Copy: {
    alignSelf: 'center',
    marginTop: 23,
    width: 198,
    height: 33,
    alignItems: 'center',
    justifyContent: 'center'
  },
  Group: {
    width: 198,
    alignItems: 'center',
    justifyContent: 'center'
  },
  Rectangle6: {
    height: 40,
    borderRadius: 5,
    borderColor: '#C5175F',
    borderWidth: 2,
    width: 198,
    alignItems: 'center',
    justifyContent: 'center'
  },
  ZudenEinstellungen: {
    backgroundColor: 'transparent',
    fontSize: 16,
    fontWeight: 'normal',
    color: '#C5175F',
    textAlign: 'center'
  }
})
